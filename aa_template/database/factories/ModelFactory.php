<?php

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'email_verified_at' => $faker->dateTimeBetween(),
        'password' => bcrypt($faker->password),
        'remember_token' => str_random(10),
    ];
});


/*
             $table->increments('id');
            $table->string('cloth_name')->unique();
            $table->string('size');
            $table->string('slug');
            $table->string('details')->nullable();
            $table->integer('new_price');
            $table->integer('old_price');
            $table->text('description');
 */

$factory->define(App\aa_ProductApparel::class, function (Faker\Generator $faker) {



    $random_int = aa_ApparelProductSeeder::$count++;

    $input = array("T shirt", "Pants", "Formal", "frock","sari");

    $size = array("XXL", "XL");

    $rand_item= array_rand($input, 1);
    $rand_size= array_rand($size, 1);

    $old_price = random_int(2000,5000);

    $new_price =  random_int($old_price-100,$old_price-10);



    $item_name = $input[$rand_item];
    return [

        'cloth_name' =>$item_name,
        'size' => $size[$rand_size],
        'slug' => str_slug($item_name." ".$random_int, "-"),
        'details' => $faker->word,
        'new_price' => $new_price,
        'old_price' => $old_price,
        'description' => $faker->text,
    ];
});

