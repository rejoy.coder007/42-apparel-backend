<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAaProductApparelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aa__product_apparels', function (Blueprint $table) {

            $table->increments('id');
            $table->string('cloth_name');
            $table->string('size');
            $table->string('slug');
            $table->string('details')->nullable();
            $table->integer('new_price');
            $table->integer('old_price');
            $table->text('description');

            //       $table->integer('category_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aa__product_apparels');
    }
}
