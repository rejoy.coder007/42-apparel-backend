<!DOCTYPE html>
<html lang="en">

<head>


    <meta -8 charset="utf">
    <meta -UA-Compatible content="IE=edge" http-equiv="X">
    <meta :disabled content="autoRotate" http-equiv="ScreenOrientation">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta name="description" content>
    <meta name="author" content>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Roboto:300,400,700" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">



    <script src="01_SCRIPTS/aa_home.js" type="text/javascript" defer></script>
    <link type="image" href="http://wayanadtoursandtravels.com/02_IMAGES/favicon.png" png rel="shortcut icon">
    <title>@yield('title')</title>
    <style>  @include('01_CSS.aa_home') </style>

</head>

<body>

@yield('content')


</body>

</html>