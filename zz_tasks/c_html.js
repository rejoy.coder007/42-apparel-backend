var gulp                                                      = require('gulp');


var  htmlmin                                          = require('gulp-htmlmin');
var gulpif                                                 = require('gulp-if');

var concat                                             = require('gulp-concat');
var VIEW_T_IN_SERVER_PART                       = '03_ViewsT/aa_WorkSpace/**/*.*';
var VIEW_T_BODY_IN_SERVER_PART                  = '03_ViewsT/aa_WorkSpace/zz_body/**/*.*';
var VIEW_T_BODY_OUT_SERVER_PART                  = 'aa_template/resources/views/zz_body';
var VIEW_OUT_SERVER_PART                        = 'aa_template/resources/views';

const htmlcomb = require('gulp-htmlcomb');

var minify = require('html-minifier').minify;

module.exports =
{
            
            
             html5_minify_view: function ()
            {
               
                var options_view = {
                    VIEW_T_IN_SERVER_PART: VIEW_T_IN_SERVER_PART,
                    VIEW_OUT_SERVER_PART: VIEW_OUT_SERVER_PART ,
                    VIEW_T_BODY_IN_SERVER_PART:VIEW_T_BODY_IN_SERVER_PART,
                    VIEW_T_BODY_OUT_SERVER_PART:VIEW_T_BODY_OUT_SERVER_PART
                };
                
                return VIEW_MOVE_HTML(options_view);

            } 

           


 
 };
 
 
 global.BODY_HTML = function (options)
{
               return gulp.src( options.VIEW_T_IN_FRAG )
                .pipe(concat('body.blade.php'))
                .pipe(gulp.dest(options.VIEW_T_OUT_BODY));

};
 
 global.VIEW_MOVE_HTML = function (options)
{






                 return   gulp.src(options.VIEW_T_IN_SERVER_PART)
                      .pipe(htmlcomb(/*options*/))

                 .pipe(gulpif(process.env.NODE_ENV === 'production',htmlmin({
                    collapseWhitespace: true,
                    removeAttributeQuotes: false,
                    removeComments: true,
                     html5:true,


                     //ignoreCustomFragments: [/@for[\s\S]*@endfor/, /@if[\s\S]*@endif/, /@php[\s\S]*@endphp/,/"{!!.*!!}"/,]
                }) ))
                .pipe(gulp.dest(options.VIEW_OUT_SERVER_PART));



};

 

  